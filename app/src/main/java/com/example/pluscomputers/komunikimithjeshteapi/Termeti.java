package com.example.pluscomputers.komunikimithjeshteapi;

public class Termeti {

        private double mMadhesia;

        private String mLokacioni;

        private long mMiliSekondat;

        public Termeti(double Madhesia, String Lokacioni, long MiliSekondat) {
            mMadhesia = Madhesia;
            mLokacioni = Lokacioni;
            mMiliSekondat = MiliSekondat;
        }

        public double getMagnitude() {
            return mMadhesia;
        }

        public String getLocation() {
            return mLokacioni;
        }

        public long getTimeInMilliseconds() {
            return mMiliSekondat;
        }

}
