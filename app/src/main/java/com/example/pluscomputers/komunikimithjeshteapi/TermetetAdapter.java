package com.example.pluscomputers.komunikimithjeshteapi;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TermetetAdapter extends RecyclerView.Adapter<TermetetAdapter.MyViewHolder>{

    private List<Termeti> listaTermeteve;
    private Context ctx;
    private Activity act;

    public TermetetAdapter(List<Termeti> listTermetet, Context ctx, Activity act) {
        this.listaTermeteve = listTermetet;
        this.ctx = ctx;
        this.act = act;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_termeti, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Termeti list = listaTermeteve.get(position);

        String txtMadhesia = new Double(list.getMagnitude()).toString();
        String txtMilisekondat = new Double(list.getTimeInMilliseconds()).toString();

        holder.madhesia.setText(txtMadhesia);
        holder.lokacioni.setText(list.getLocation());
        holder.milisekondat.setText(txtMilisekondat);
    }

    @Override
    public int getItemCount() {
        return listaTermeteve.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView madhesia,lokacioni,milisekondat;
        public ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);

            madhesia = itemView.findViewById(R.id.madhesia);
            lokacioni = itemView.findViewById(R.id.lokacioni);
            milisekondat = itemView.findViewById(R.id.milisekondat);

        }
    }
}
