package com.example.pluscomputers.komunikimithjeshteapi;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public final class Query {

    private Query() {
    }

    public static ArrayList<Termeti> shfaqTermetet(JSONObject response) {

        ArrayList<Termeti> listTermeteve = new ArrayList<>();

        try {

            JSONArray earthquakeArray = response.getJSONArray("features");

            for (int i = 0; i < earthquakeArray.length(); i++) {

                JSONObject termetiAktual = earthquakeArray.getJSONObject(i);

                JSONObject properties = termetiAktual.getJSONObject("properties");

                double madhesia = properties.getDouble("mag");

                String lokacioni = properties.getString("place");

                long milisekondat = properties.getLong("time");

                Termeti objTermeti = new Termeti(madhesia, lokacioni, milisekondat);

                listTermeteve.add(objTermeti);
            }

        } catch (JSONException e) {
            Log.e("QueryUtils", "Problem parsing the earthquake JSON results", e);
        }

        return listTermeteve;
    }

}

